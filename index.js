

// //////
function bai1() {
    var content = ''
    for (var i = 0; i < 10; i++) {
        content += `<tr>`
        for (var k = 1; k <= 10; k++) {
            content += `<td>${i * 10 + k}</td>`
        }
        content += `</tr>`
    }
    var html = `<table border="1" style="text-align: right">${content}</table>`
    document.getElementById('bai1').innerHTML = html;
}


// input mảng
var numArr = [];
function pushItem() {
    var item = document.getElementById('addItem').value * 1;
    numArr.push(item);
    document.getElementById('m2').innerText = numArr.toString();
    document.getElementById('addItem').value = '';
    document.getElementById('addItem').focus();
}
function clearArr() {
    numArr = [];
    document.getElementById('m2').innerText = numArr.toString();
}
// Bai 2
function laSoNguyenTo(n) {
    if (n > 1
        && (n === 2
            || n === 3
            || n === 5
            || n === 7
            || (n % 2 !== 0
                && n % 3 !== 0
                && n % 5 !== 0
                && n % 7 !== 0))
    ) {
        return true
    }
    return false
}
function timCacSoNguyenTo(ar) {
    var ketQua = [];
    for (var i = 0; i < ar.length; i++) {
        if (laSoNguyenTo(ar[i])) {
            ketQua.push(ar[i])
        }
    }
    return ketQua;
}
function bai2() {
    document.getElementById('bai2').innerText = timCacSoNguyenTo(numArr).toString()
}

function tinhS(n) {
    var s = 0;
    for (var i = 2; i <= n; i++) {
        s += i;
    }
    return s + 2 * n;
}
function bai3() {
    var n = document.getElementById('input3').value * 1;
    document.getElementById('bai3').innerText = tinhS(n).toString()
}

function timUoc(n) {
    var ketQua = [];
    for (var i = n; i > 0; i--) {
        if (n % i === 0) {
            ketQua.push(i)
        }
    }
    return ketQua
}
function bai4() {
    var n = document.getElementById('input4').value * 1;
    document.getElementById('bai4').innerText = timUoc(n).toString()
}

function soDaoNguoc(n) {
    var kq = n.toString().split('').reverse().join('') * 1;
    return kq
}
function bai5() {
    var n = document.getElementById('input5').value * 1;
    document.getElementById('bai5').innerText = soDaoNguoc(n).toString()
}


function soDuongLonNhat(n) {
    var tong = 0;
    var x = 0;
    while (tong <= n) {
        x++;
        tong += x;
    }
    return --x;
}
function bai6() {
    document.getElementById('bai6').innerText = soDuongLonNhat(100).toString()
}

// bài 7
function cuuChuong(n) {
    var kq = '';
    for (let i = 0; i <= 10; i++) {
        kq += (`${n} x ${i} = ${n * i} <br>`);
    }
    return kq;
}
function bai7() {
    var n = document.getElementById('input7').value * 1;
    document.getElementById('bai7').innerHTML = cuuChuong(n)
}

// bài 8
function chiaBai() {
    var players = [[], [], [], []];
    var cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S", "AS", "7H", "9K", "10D"]

    for (var i = 0; i < cards.length; i++) {
        players[i % 4].push(cards[i])
    }

    var kq = ''
    for (var i = 0; i < players.length; i++) {
        kq += players[i].toString() + '<br>'
    }
    return kq;
}
function bai8() {
    document.getElementById('bai8').innerHTML = chiaBai()
}

function gaCho(soCon, soChan) {
    var soGa = -1;
    var soCho = -1;
    //  soGa * 2 + soCho * 4 = soChan
    //  soGa + soCho = soCon
    // => soCho = soCon - soGa
    // => soGa * 2 + (soCon - soGa) * 4 = soChan
    // => soGa = (soCon * 4 - soChan)/2
    soGa = (soCon * 4 - soChan) / 2;
    soCho = soCon - soGa;
    return `${soGa} gà, ${soCho} chó`
}
function bai9() {
    var soCon = document.getElementById('soCon').value * 1;
    var soChan = document.getElementById('soChan').value * 1;
    document.getElementById('bai9').innerText = gaCho(soCon, soChan);
}

// bài 10
function gocLech(soGio, soPhut) {
    if (soGio > 12 || soPhut > 59 || soGio < 0 || soPhut < 0) {
        console.log('giờ phút ko đúng');
        return;
    }
    var gio = soGio === 12 ? 0 : soGio;
    var doPhut = soPhut * 6;
    var doGio = gio * 30 + soPhut * 0.5; // có thêm số giây sẽ rắc rối hơn chút
    var gocLech = Math.abs(doPhut - doGio);
    return gocLech;
}
function bai10() {
    var gio = document.getElementById('gio').value * 1;
    var phut = document.getElementById('phut').value * 1;
    document.getElementById('bai10').innerText = gocLech(gio, phut);
}